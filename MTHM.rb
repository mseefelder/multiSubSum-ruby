require "rubygems"
require "json"


#data structures
element = Struct.new(:index, :id, :value, :belong)
bin = Struct.new(:index, :name, :limit, :remain)

#read files
winners = JSON.parse(open("winners.json").read)
loot = JSON.parse(open("items.json").read)

all_items = loot.map.with_index { |e, i| element.new(i,e["id"],e["price"].to_f,nil) }
all_players = winners.map.with_index { |e, i| bin.new(i,e["user"],e["total"],e["total"]) }

#bad test case
#all_items = [element.new(0,11,0.5,nil), element.new(1,32,0.3,nil), element.new(2,46,0.5,nil), element.new(3,98,0.7,nil), element.new(4,44,0.02,nil)]
#all_players = [bin.new(0,"Ab",1.02,1.02), bin.new(1,"Bo",1.0,1.0)]

#functions
def reindex_elements (arr)
	arr.each_with_index do |x,i|
		x[:index] = i
	end
end

def greedys (items,players)
	players.each do |player|
		items.each do |item|
			if item[:belong] == nil
				if player[:remain] - item[:value] >= 0
					item[:belong] = player[:index]
					player[:remain] = player[:remain] - item[:value]
				end
			end
		end
	end	
end

def marcos (items,players)
	item = items.select { |e| e[:belong] == nil  }.reverse[0]
	puts item

	player_possible = players.select { |e| e[:limit]-e[:remain] >= item[:value]  }.sort {|x,y| items.select { |e| e[:belong] == x[:index] }.reverse[0][:value] <=> items.select { |e| e[:belong] == y[:index] }.reverse[0][:value] }#{ |x,y| (y[:limit]-y[:remain]) <=> (x[:limit]-x[:remain]) }
	
	player_possible.each do |p|
		puts p[:name]
		puts p[:index]
		puts items.select { |e| e[:belong] == p[:index]  }.length
	end

	puts "over"
	
	player = player_possible[0]
	#items_sort_belong = all_items.select { |e| e[:belong] != nil  }.select { |e| e[:value] < item[:value]  }.sort { |x,y| y[:belong] <=> x[:belong] }
	items.select { |e| e[:belong] == player[:index] }.each do |i|
		puts i
		i[:belong] = nil
	end
	item[:belong] = player[:index]
	player[:remain] = player[:limit]-item[:value]
end

def rearrange (items,players)
	players.each do |player|
		player[:remain] = player[:limit]
	end
	curr_index = 0
	items.reverse_each do |item|
		assigned = false
		if item[:belong] != nil

			players.drop(curr_index).each do |player|
				if item[:value] < player[:remain]
					item[:belong] = player[:index]
					player[:remain] = player[:remain] - item[:value]
					if curr_index < players.length
						curr_index = curr_index + 1
					else
						curr_index = 0
					end
					assigned = true
					break
				end
			end
			if assigned == true
				next
			end
			players.take(curr_index).each do |player|
				if item[:value] < player[:remain]
					item[:belong] = player[:index]
					player[:remain] = player[:remain] - item[:value]
					if curr_index < players.length
						curr_index = curr_index + 1
					else
						curr_index = 0
					end
					assigned = true
					break
				end
			end
			if assigned == true
				next
			end
			item[:belong] = nil

		end
	end

end

def fimp (items, players)
	#Find minimum-cost unassigned item
	minimum_unassigned = items.reverse.each do |item|
		break item[:index] if item[:belong] == nil
	end
	#Make first improvement
	items.each do |item|
		items.drop(item[:index]+1).each do |inner_item|
			if item[:belong] != inner_item[:belong] and item[:belong] != nil and inner_item[:belong] != nil
				maximum = item[:value] > inner_item[:value] ? item[:index] : inner_item[:index] 
				minimum = item[:value] < inner_item[:value] ? item[:index] : inner_item[:index] 
				difference = items[maximum][:value] - items[minimum][:value]
				if difference <= players[items[minimum][:belong]][:remain] and players[items[maximum][:belong]][:remain] + difference >= items[minimum_unassigned][:value]
					#get unassigned item with higher possible value that fits in item exchange
					found_better = false
					better = items.reverse.drop(items.length - minimum_unassigned).each do |t_unass_item|
						if t_unass_item[:value] > players[items[maximum][:belong]][:remain] + difference
							break
						end
						if t_unass_item[:belong] == nil
							found_better = true
						end
						break t_unass_item[:index] if t_unass_item[:belong] == nil
					end
					unass_item = found_better ? better : minimum_unassigned
					#exchange items and assign unassigned item to former higher valued item owner
					players[items[maximum][:belong]][:remain] = players[items[maximum][:belong]][:remain] + difference - items[unass_item][:value]
					players[items[minimum][:belong]][:remain] = players[items[minimum][:belong]][:remain] - difference
					items[unass_item][:belong] = items[maximum][:belong]
					items[maximum][:belong] = items[minimum][:belong]
					items[minimum][:belong] = items[unass_item][:belong]
					#if minimum_unassigned was assigned, find next
					if !found_better
						minimum_unassigned = items.take(minimum_unassigned).reverse.each do |item|
							break item[:index] if item[:belong] == nil
						end
					end
				end
			end
		end
	end
end

def simp (items,players)
	items.reverse.each do |item_remove|
		next if item_remove[:belong] == nil
		limit = players[item_remove[:belong]][:remain] + item_remove[:value]
		stash = []
		items.each do |e|
			if e[:belong] == nil and e[:value] <= limit
				stash.push(e[:index])
				limit = limit - e[:value]
			end
		end
		if stash.map{ |e| items[e][:value] }.inject(0, &:+) > item_remove[:value]
			stash.each do |item_index_add|
				items[item_index_add][:belong] = item_remove[:belong]
			end
			players[item_remove[:belong]][:remain] = limit
			item_remove[:belong] = nil
		end
	end
end

def status(i, p)
	unassigneds = i.select { |e| e[:belong]==nil }.map { |e| e[:value]  }
	puts "Players total remain: " + p.map { |e| e[:remain]  }.inject(0, &:+).to_s
	puts unassigneds.count.to_s + " items total remain: " + unassigneds.inject(0, &:+).to_s
end

#Here it goes
puts "Let's go"

#Sort items in crescent value order
all_items.sort! { |x,y| y[:value] <=> x[:value] }
#all_players.sort! { |x,y| y[:limit] <=> x[:limit] }
reindex_elements(all_items)
#reindex_elements(all_players)
#Greedys
greedys(all_items,all_players)
puts "First step"
status(all_items,all_players)
puts all_items
puts all_players

marcos(all_items,all_players)
puts "Marcos"
status(all_items,all_players)

greedys(all_items,all_players)
puts "First step"
status(all_items,all_players)

=begin
#Rearrange
rearrange(all_items,all_players)
puts "Rearranged"
status(all_items,all_players)
#Greedys again
greedys(all_items,all_players)
puts "Second step"
status(all_items,all_players)
#first improvement
fimp(all_items,all_players)
puts "First improvement"
status(all_items,all_players)
#second improvement
simp(all_items,all_players)
puts "Second Improvement"
status(all_items,all_players)

#all_items.each do |i|

#end

=end